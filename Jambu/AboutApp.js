import React, { useEffect } from 'react'
import { StyleSheet, Text, View, Button, Image, TouchableOpacity } from 'react-native'
import { useFonts, Inter_900Black } from '@expo-google-fonts/inter';

export default function AboutApp({navigation}) {
    return (
        <View style={styles.container}>
            <View style={styles.subContainer}>
                <Image
                    style={styles.tinyLogo}
                    source={require('./asset/logo.png')}
                />
                <Image
                    style={styles.image}
                    source={require('./asset/image1.jpg')}
                />
                <Text style={styles.txtDesc}>Jambu (Pinjam Buku) merupakan aplikasi pemijaman buku online, yang mana bagi yang akan meminjam buku bisa memilih buku lalu jambu akan mengirimkan bukunya ke rumah mu.</Text>
                <Text style={styles.txtDesc}>Bagi yang ingin menjadi kontributor bisa menghubungi jambu melalui email jambu@gmail.com</Text>
            </View>

            <TouchableOpacity onPress={()=>navigation.navigate("Login")} style={{height:61}}>
                <View style={styles.bottom}>
                    <Text style={styles.txtBottom}>LANJUTKAN</Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'flex-start',
        alignItems:'stretch',
        backgroundColor:'#ffffff'
    },
    subContainer:{
        flex:1,
        justifyContent:'space-evenly',
        alignItems:'center'
    },
    tinyLogo: {
        width: 120,
        height: 40,
        alignItems:'center'
    },
    image: {
        width: 400,
        height: 300,
    },
    txtDesc:{
        paddingLeft:4,
        paddingRight:4,
        textAlign:'center',
        fontFamily: 'Inter_900Black',
        fontSize:16
    },
    bottom:{
        flex:1,
        alignItems:'stretch',
        backgroundColor:'#09203F',
        alignItems:'center',
        height:50
    },
    txtBottom:{
        color:'#ffffff',
        padding:16,
        fontSize:20
    }
})