import React, { useEffect } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
const Splash = ({navigation}) => {
    useEffect(()=>{
        setTimeout(()=>{
            navigation.navigate("AboutApp")
        }, 4000)
    }, [])

    return (
        <View style={{justifyContent:'center', alignItems:'center', flex:1, backgroundColor:'#E6E9F0'}}>
           <Image
                style={styles.tinyLogo}
                source={require('./asset/logo.png')}
            />
        </View>
    )
}

export default Splash

const styles = StyleSheet.create({
    tinyLogo: {
        width: 300,
        height: 75,
    },
})