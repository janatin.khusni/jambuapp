import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';

import AboutApp from '../AboutApp';
import AboutMe from '../AboutMe';
import Cart from '../Cart';
import Login from '../Login';
import Home from '../Home';
import Product from '../Product'; 
import Registrasi from '../Registrasi';
import Splash from '../Splash';
import Thanks from '../Thanks';

const Tab = createBottomTabNavigator();
const Drawwer = createDrawerNavigator();
const Stack = createStackNavigator();

export default function Router() {
    return (
      <NavigationContainer>
          <Stack.Navigator headerMode="none">
              <Stack.Screen name="Splash" component={Splash}/>
              <Stack.Screen name="AboutApp" component={AboutApp}/>
              <Stack.Screen name="Login" component={Login}/>
              <Stack.Screen name="Registrasi" component={Registrasi}/>
              <Stack.Screen name="Product" component={Product}/>
              <Stack.Screen name="Thanks" component={Thanks}/>
              <Stack.Screen name="MainApp" component={MainApp}/>
              <Stack.Screen name="MyDrawwer" component={MyDrawwer}/>
          </Stack.Navigator>
      </NavigationContainer>
    )
}

const MainApp =()=>(    
        <Tab.Navigator>
            <Tab.Screen name="Home" component={Home}/>
            <Tab.Screen name="Cart" component={Cart}/>
            {/* <Tab.Screen name="AboutMe" component={AboutMe}/> */}
        </Tab.Navigator>    
)

const MyDrawwer =()=>(
    <Drawwer.Navigator>
        <Drawwer.Screen name="App" component={MainApp}/>
        <Drawwer.Screen name="AboutMe" component={AboutMe} />
    </Drawwer.Navigator>
)