import axios from 'axios'
import React, {useState, useEffect} from 'react'
import { StyleSheet, Text, View, Button, Image, TextInput, Icon, TouchableOpacity, ScrollView } from 'react-native'

const Item = ({title, autor, desc, image, onPress}) => {
    return (
        <TouchableOpacity style={styles.messageContainer} onPress={onPress}>
            <View style={{flexDirection:'row'}}>   
                <Image
                    style={{height: 80, width: 50}}
                    source={{uri:image}}
                />
                <View style={{paddingLeft:12}}>
                    <Text style={styles.textTitle}>{title}</Text>
                    <Text style={styles.textCreator} numberOfLines={1}>{autor}</Text>
                    <Text style={styles.textDesc} numberOfLines={3}>{desc}</Text>
                </View>
            </View>
        </TouchableOpacity> 
    )
}

export default function Home({navigation}) {
    const [search, setSearch] = useState("buku islami");
    const [books, setBooks] = useState([]);
    var imageNotAvailable = 'https://upload.wikimedia.org/wikipedia/commons/0/0a/No-image-available.png'

    useEffect(() => {
        getData();
    }, [])

    const getData = () => {
        axios.get(`https://www.googleapis.com/books/v1/volumes?q=${search}`)
        .then(res => {
            console.log('getData: ', res.data.items)
            setBooks(res.data.items);
        })
    }

    const selectItem = (book) => {
        console.log('selected item: ',book)
    }
    
    return (
        <ScrollView>
            <View style={styles.container}>
                <Image
                    style={styles.tinyLogo}
                    source={require('./asset/logo.png')}
                />
                <View style={styles.subContainer}>
                    <View style={{backgroundColor:'white', flexDirection:'row',alignItems:'center'}}>
                        <TextInput 
                            value={search}
                            onChangeText={(value)=>setSearch(value)}
                            style={styles.input}
                        ></TextInput>
                        <TouchableOpacity style={{padding: 10}} onPress={() => getData()}>
                            <Image
                                style={{width: 20, height: 20}}
                                source={require('./asset/search.png')}
                            />
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity style={{height:40, alignItems:'center'}} onPress={() => navigation.navigate("MyDrawwer",{
                        screen : 'App', params:{
                        screen:'Cart'
                        }
                    })}>
                        <View style={{marginLeft:10,alignItems:'center',flex:1,justifyContent:'center',flexDirection:'row'}}>
                            <Image
                                style={{width: 30,height: 30, }}
                                source={require('./asset/cart.png')}
                            />
                            <Text style={{widht:10,marginTop:-30, backgroundColor:'#f84f31', color:'white', borderRadius:10, paddingVertical:2, paddingHorizontal:6, fontSize:10, marginLeft:-12}}>1</Text>
                        </View>
                    </TouchableOpacity>
                </View>

            {books.map(book => {
                var imageLink = ''
                if (book.volumeInfo.imageLinks == undefined) {
                    imageLink = imageNotAvailable
                }else{
                    imageLink = book.volumeInfo.imageLinks.smallThumbnail
                }

                return <Item 
                key={book.id} 
                title={book.volumeInfo.title} 
                autor={book.volumeInfo.authors} 
                desc={book.volumeInfo.description}
                image={imageLink}
                
                onPress={() => navigation.navigate("Product")}
            />
            })}
                </View>
            
            </ScrollView>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:'center' ,
        backgroundColor:'#E6E9F0',
        justifyContent:'flex-start',
        flexDirection:'column',
        paddingTop:10
    },
    tinyLogo: {
        width: 90,
        height: 30,
        alignItems:'center',
        marginTop: 40
    },
    subContainer:{
        width: 300,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop:20,
        marginBottom:12,
    },
    input:{
        fontFamily: 'Inter_900Black',
        flex: 1,
        padding: 10,
        backgroundColor: '#fff',
    },
    messageContainer:{
        backgroundColor: 'white',
        flexDirection:'row',
        padding: 12,
        justifyContent:'space-between',
        marginHorizontal:16,
        width:350
    },
    textTitle:{
        fontFamily: 'Inter_900Black',
        fontWeight: 'bold',
        color:'#537895',
        width: 260
    },
    textCreator:{
        fontFamily: 'Inter_900Black',
        color:'#09203F',
        width: 260
    },
    textDesc:{
        fontFamily: 'Inter_900Black',
        color:'#000000',
        width: 260,
    }
})