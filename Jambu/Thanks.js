import React, { useEffect } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
const Thanks = ({navigation}) => {
    useEffect(()=>{
        setTimeout(()=>{
            navigation.navigate("MyDrawwer",{
                screen : 'App', params:{
                    screen:'Home'
                }
            })
        }, 4000)
    }, [])

    return (
        <View style={{justifyContent:'center', alignItems:'center', flex:1, backgroundColor:'#E6E9F0'}}>
           <Image
                style={styles.tinyLogo}
                source={require('./asset/logo.png')}
            />
            <Text style={styles.textTitle}>Berhasil sewa buku, silakan tunggu buku anda sampai</Text>
        </View>
    )
}

export default Thanks

const styles = StyleSheet.create({
    tinyLogo: {
        width: 300,
        height: 75,
    },
    textTitle:{
        fontFamily: 'Inter_900Black',
        fontWeight: 'bold',
        color:'#537895',
        fontSize: 20,
        textAlign:'center',
        padding:20,
    }
})