import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, Button, Image, TextInput, TouchableOpacity } from 'react-native'
import * as firebase from 'firebase'

export default function Registrasi({navigation}) {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [passwordC, setPasswordC] = useState("");
    const [error, setError] = useState("");

    const firebaseConfig = {
        apiKey: "AIzaSyC5-kRHca2vxNq-_IAE_vlPKOps5QAvRw8",
        authDomain: "sanber-1e28a.firebaseapp.com",
        projectId: "sanber-1e28a",
        storageBucket: "sanber-1e28a.appspot.com",
        messagingSenderId: "436059631477",
        appId: "1:436059631477:web:cdf8f3d58e3beb65bced3c",
        measurementId: "G-NMVQDGEQTP"
    };
    if(!firebase.apps.length){
        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);
    }
    const submit = () => {
        const data = {
            email, password, passwordC
        }
        console.log(data)
        if (password == passwordC) {
            firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(()=>{
                console.log("Register Berhasil")
                navigation.navigate("MyDrawwer",{
                    screen : 'App', params:{
                        screen:'HomeScreen'
                    }
                })
            }).catch(()=>{
                console.log("Register Gagal")
                setError('Register Gagal')
            })
        }else{
            setError('Konfirmasi Password Beda')
        }
        
    }
    return (
        <View style={styles.container}>
            <Image
                style={styles.tinyLogo}
                source={require('./asset/logo.png')}
            />
            <View>
                <View style={{alignItems:'flex-start',flexDirection: 'column'}}>
                    <Text style={{fontFamily: 'Inter_900Black'}}>Email</Text>
                    <TextInput 
                        value={email}
                        onChangeText={(value)=>setEmail(value)}
                        style={styles.input}
                    ></TextInput>
                </View>
                <View style={{alignItems:'flex-start',flexDirection: 'column',marginTop:40}}>
                    <Text style={{fontFamily: 'Inter_900Black'}}>Kata Sandi</Text>
                    <TextInput 
                        value={password}
                        onChangeText={(value)=>setPassword(value)}
                        style={styles.input}
                        secureTextEntry={true}
                    ></TextInput>
                </View>
                <View style={{alignItems:'flex-start',flexDirection: 'column',marginTop:40}}>
                    <Text style={{fontFamily: 'Inter_900Black'}}>Konfirmasi Kata Sandi</Text>
                    <TextInput 
                        value={passwordC}
                        onChangeText={(value)=>setPasswordC(value)}
                        style={styles.input}
                        secureTextEntry={true}
                    ></TextInput>
                </View>
                <TouchableOpacity 
                    style={{marginTop:40}}
                    onPress={submit}
                >
                    <View style={styles.bottom}>
                        <Text style={styles.txtBottom}>Daftar</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <Text style={{fontFamily: 'Inter_900Black',color:'red'}}>{error}</Text>
            <View style={{alignItems:'center',flexDirection: 'row'}}>
                <Text style={styles.txtDesc}>Sudah punya akun JAMBU?</Text>
                <TouchableOpacity onPress={()=>navigation.navigate("Login")}
                    style={{padding:4}}>
                    <Text style={styles.txtRegis}>Masuk</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'space-evenly',
        alignItems:'center',
        backgroundColor:'#ffffff'
    },
    tinyLogo: {
        width: 120,
        height: 40,
        alignItems:'center'
    },
    image: {
        width: 400,
        height: 300,
    },
    txtDesc:{
        fontFamily: 'Inter_900Black',
        fontSize:16
    },
    txtRegis:{
        fontFamily: 'Inter_900Black',
        fontSize:16,
        fontWeight: 'bold',
        color:'#09203F'
    },
    bottom:{
        alignItems:'stretch',
        backgroundColor:'#09203F',
        alignItems:'center',
        borderRadius:4
    },
    txtBottom:{
        color:'#ffffff',
        padding:12,
        fontSize:20,
        fontFamily: 'Inter_900Black'
    },
    input:{
        fontFamily: 'Inter_900Black',
        padding:10,
        width: 300,
        height: 50,
        borderColor:'#09203F',
        borderWidth:1,
        borderRadius:4
    }
})