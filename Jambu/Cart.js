import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, ScrollView, TextInput, TouchableOpacity, Alert } from 'react-native'

export default function Cart({navigation}) {
    const [hari, setHari] = useState("1");
    const [telp, setTelp] = useState("");
    const [alamat, setAlamat] = useState("");
    const [biayaBukuPerHari, setBiayaBukuPerHari] = useState("869");//biaya keseluruhan buku perhari
    const [biayaBuku, setBiayaBuku] = useState("");//biaya keseluruhan buku perhari x jumlah hari
    const [ongkir, setOngkir] = useState("1");//ongkir 10k/3 buku
    const [operasional, setOperasional] = useState("1");//biaya operasional 5k/buku
    const [jumlahBuku, setJumlahbuku] = useState("1");// banyak buku yang di sewa
    const [total, setTotal] = useState("1");//
    // const hitungBiayaBuku = () => {

    // }
    const hitung = () => {
        var totalHitungan = (hari * biayaBukuPerHari) + ((jumlahBuku%3)*10000) + (jumlahBuku*5000)
        setBiayaBuku(hari * biayaBukuPerHari)
        setOngkir((jumlahBuku%3)*10000)
        setOperasional(jumlahBuku*5000)
        console.log('biaya '+totalHitungan)
        setTotal(totalHitungan)
    }
    
    useEffect(() => {
        hitung();
    }, [])

    return (
        <View style={styles.container}>
            <Text style={styles.textTitle}>Kita Pernah Salah</Text>
            <ScrollView>
                <View style={{paddingHorizontal:20, paddingVertical:12}}>
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        <Text style={styles.text}>Lama Waktu Pinjam</Text>
                        <TextInput 
                            value={hari}
                            onChangeText={(value)=>{
                                setHari(value.replace(/[^1-9]/g, ''))
                                if (value > 0) {
                                    setBiayaBuku(value * biayaBukuPerHari)
                                }
                            }}
                            style={styles.input}></TextInput>
                        <Text style={styles.text2}>Hari</Text>
                    </View>
                    <View style={styles.subContainer2}>
                        <Text style={{fontFamily: 'Inter_900Black'}}>Nomor Telepon</Text>
                        <TextInput 
                            value={telp}
                            onChangeText={(value)=>setTelp(value)}
                            style={styles.input2}
                        ></TextInput>
                    </View>
                    <View style={styles.subContainer2}>
                        <Text style={{fontFamily: 'Inter_900Black'}}>Alamat</Text>
                        <TextInput 
                            value={alamat}
                            onChangeText={(value)=>setAlamat(value)}
                            style={styles.input2}
                        ></TextInput>
                    </View>
                    <View style={styles.subContainer2}>
                        <Text style={{fontFamily: 'Inter_900Black'}}>Buku yang dipinjam</Text>
                        <View style={styles.input2}>
                            <Text style={styles.textTitle2}>Keranjang jambuku</Text>
                            <Text style={styles.text3}>Rp {biayaBukuPerHari}/hari</Text>
                        </View>
                    </View>
                    <View style={styles.subContainer2}>
                        <Text style={{fontFamily: 'Inter_900Black'}}>Biaya sewa buku</Text>
                        <View style={styles.input2}>
                            <View style={{flexDirection:'row', alignItems:'center'}}>
                                <Text style={styles.text}>Total sewa buku perhari</Text>
                                <Text style={styles.text2}>{biayaBukuPerHari}</Text>
                            </View>
                            <View style={{flexDirection:'row', alignItems:'center'}}>
                                <Text style={styles.text}>Lama waktu pinjam (hari)</Text>
                                <Text style={styles.text2}>{hari}</Text>
                            </View>
                            <View style={{flexDirection:'row', alignItems:'center'}}>
                                <Text style={styles.text4}>Total biaya pinjam buku</Text>
                                <Text style={styles.text5}>Rp {biayaBuku}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.subContainer2}>
                        <Text style={{fontFamily: 'Inter_900Black'}}>Total biaya sewa buku</Text>
                        <View style={styles.input2}>
                            <View style={{flexDirection:'row', alignItems:'center'}}>
                                <Text style={styles.text}>Biaya pinjam buku</Text>
                                <Text style={styles.text2}>{biayaBuku}</Text>
                            </View>
                            <View style={{flexDirection:'row', alignItems:'center'}}>
                                <Text style={styles.text}>Ongkir (10k/3 buku)</Text>
                                <Text style={styles.text2}>{ongkir}</Text>
                            </View>
                            <View style={{flexDirection:'row', alignItems:'center'}}>
                                <Text style={styles.text}>Biaya Operasional (5k/buku)</Text>
                                <Text style={styles.text2}>{operasional}</Text>
                            </View>
                            <View style={{flexDirection:'row', alignItems:'center'}}>
                                <Text style={styles.text4}>Total biaya pinjam buku</Text>
                                <Text style={styles.text5}>Rp {total}</Text>
                            </View>
                        </View>
                    </View>
                    <TouchableOpacity 
                    style={{marginTop:40, alignItems:'stretch'}}
                    onPress={() => navigation.navigate("Thanks")}
                    >
                    <View style={styles.bottom}>
                        <Text style={styles.txtBottom}>JAMBU</Text>
                    </View>
                </TouchableOpacity>
                </View>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: 'flex-start',
        alignItems:'stretch',
        flexDirection:'column',
        backgroundColor:'#E6E9F0',
        paddingTop:10
    },
    subContainer:{
        height:50,
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'white'
    },
    subContainer2:{
        alignItems:'stretch',
        flexDirection: 'column', 
        marginTop:10,
    },
    textTitle:{
        fontFamily: 'Inter_900Black',
        fontWeight: 'bold',
        color:'#537895',
        fontSize: 20,
        textAlign:'center',
        padding:20,
        backgroundColor:'white',
        marginTop:12
    },
    textTitle2:{
        fontFamily: 'Inter_900Black',
        fontWeight: 'bold',
        color:'#537895',
        fontSize: 16,
    },
    text:{
        fontFamily: 'Inter_900Black',
        flex:1,
        textAlignVertical: 'center',
        color:'#09203F'
    },
    text2:{
        fontFamily: 'Inter_900Black',
        marginLeft: 12,
        color:'#09203F'
    },
    text3:{
        fontFamily: 'Inter_900Black',
        marginTop: 4,
        color:'#09203F'
    },
    text4:{
        fontFamily: 'Inter_900Black',
        flex:1,
        fontWeight: 'bold',
        textAlignVertical: 'center',
        color:'#09203F'
    },
    text5:{
        fontFamily: 'Inter_900Black',
        fontWeight: 'bold',
        marginLeft: 12,
        color:'#09203F'
    },
    input:{
        fontFamily: 'Inter_900Black',
        height:30,
        width:30,
        backgroundColor:'white',
        textAlign: 'center',
        borderRadius: 4,
        borderColor: '#D2D2D2',
        borderWidth:1
    },
    input2:{
        fontFamily: 'Inter_900Black',
        backgroundColor:'white',
        borderRadius: 4,
        borderColor: '#D2D2D2',
        borderWidth:1,
        flex:1,
        padding:10
    },
    bottom:{
        flex:1,
        alignItems:'stretch',
        backgroundColor:'#09203F',
        alignItems:'center',
        borderRadius:4
    },
    txtBottom:{
        color:'white',
        padding:12,
        fontSize:20,
        fontFamily: 'Inter_900Black'
    },
})