import React from 'react'
import { StyleSheet, Text, View, Button, Image, TouchableOpacity } from 'react-native'

export default function Product({navigation}) {
    var image = 'http://books.google.com/books/content?id=4Q6zDwAAQBAJ&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api'
    return (
        <View style={styles.container}>
            <View style={styles.tab}>
                <TouchableOpacity onPress={()=>
                    navigation.navigate("MyDrawwer",{
                        screen : 'App', params:{
                            screen:'Home'
                        }
                    })
                }>
                    <Image
                        style={styles.back}
                        source={require('./asset/back.png')}
                    />
                </TouchableOpacity>
                <Text style={styles.textTitle}>Kita Pernah Salah</Text>
                <TouchableOpacity style={{height:40, alignItems:'center'}} onPress={() => navigation.navigate("MyDrawwer",{
                    screen : 'App', params:{
                    screen:'Cart'
                    }
                })}>
                    <View style={{marginLeft:10,alignItems:'center',flex:1,justifyContent:'center',flexDirection:'row'}}>
                        <Image
                            style={{width: 30,height: 30, }}
                            source={require('./asset/cart.png')}
                            />
                            <Text style={{widht:10,marginTop:-30, backgroundColor:'#f84f31', color:'white', borderRadius:10, paddingVertical:2, paddingHorizontal:6, fontSize:10, marginLeft:-12}}>1</Text>
                        </View>
                    </TouchableOpacity>
            </View>
            <View style={{alignItems:'center'}}>
                <Image
                    style={styles.image}
                    source={{uri:image}}
                />
            </View>
            <View style={styles.tab}>
                <View style={{flex:1, justifyContent:'center'}}>
                    <Text style={styles.textDesc}>Rp 869/hari</Text>
                    <Text style={styles.textCreator}>@fuadbakh</Text>
                </View>
                <TouchableOpacity onPress={()=>
                    navigation.navigate("MyDrawwer",{
                        screen : 'App', params:{
                            screen:'Home'
                        }
                    })
                } >
                    <Image
                        style={{height:40, width:40}}
                        source={require('./asset/cart2.png')}
                    />
                </TouchableOpacity>
            </View>
            <Text style={styles.textDesc2}>Jika Kita Tak Pernah Jadi Apa-Apa Kau melihat teman-teman dan mereka sudah sudah mendapatkan impian, sementara kau masih termangu menggenggam harapan. Pelan, dalam hati kau berujar, \"Kapan mimpiku terwujud?\" *** Jika Kita Tak Pernah Jadi Apa-Apa Selama perjalanan mencapai tujuan, adakalanya kau melihat sekeliling... menakar jauh jangkauan. Atau, kau malah membandingkannya dengan orang lain. Lalu, lupa melanjutkan perjalanan. *** Jika Kita Tak Pernah Jadi Apa-Apa Benarkah segala usaha dan upayamu selama ini lebur bersama kecewa yang kau bangun sendiri? Sungguhkah sesuatu yang hanya kau lihat dalam dunia maya menjadikanmu merasa bukan apa-apa? *** Jika Kita Tak Pernah Jadi Apa-Apa akan menemanimu selama perjalanan. Buku ini untukmu yang khawatir tentang masa depan. Tenang saja, kau tidak sedang diburu waktu. Bacalah tiap lembarnya dengan penuh kesadaran bahwa hidup adalah tentang sebaik-baiknya berusaha, jatuh lalu bangun lagi, dan tidak berhenti percaya bahwa segala perjuanganmu tidak akan sia-sia. Bukankah sebaiknya apa-apa yang fana tidak selayaknya membuatmu kecewa? ---------------Sebuah buku inspiratif untuk pengembangan diri persembahan penerbit Gagasmedia. Buku persembahan penerbit GagasMedia #DirumahAja</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: 'flex-start',
        alignItems:'stretch',
        flexDirection:'column',
        padding:20,
        backgroundColor:'white',
    },
    tab:{
        height:50,
        flexDirection:'row',
        justifyContent:'flex-start',
        alignItems:'center',
        paddingTop:30
    },
    back:{
        height:20,
        width:30
    },
    textTitle:{
        fontFamily: 'Inter_900Black',
        fontWeight: 'bold',
        color:'#537895',
        fontSize: 18,
        flex:1,
        marginLeft:20,
        marginRight:20
    },
    image:{
        height:200,
        width:130,
        marginTop:10
    },
    textCreator:{
        fontFamily: 'Inter_900Black',
        color:'#09203F',
        width: 220,
        marginTop:4
    },
    textDesc:{
        fontFamily: 'Inter_900Black',
        color:'#000000',
    },
    textDesc2:{
        fontFamily: 'Inter_900Black',
        color:'#000000',
        marginTop:20
    }
})