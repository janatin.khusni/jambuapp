import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'

export default function AboutMe() {
    return (
        <View style={styles.container}>
            <View style={{justifyContent:'center', backgroundColor:'#E6E9F0', padding:20, flexDirection:'row'}}>
                <Image
                    style={styles.image}
                    source={require('./asset/image2.png')}
                />
                <View style={styles.container2}>
                    <Text style={styles.text}>Janatin Khusni</Text>
                    <Text style={styles.text}>Android Dev</Text>
                </View>
            </View>
            <View style={{flexDirection:'row', padding:20}}>
                <Text style={styles.text2}>Saldo jambumu</Text>
                <Text style={styles.text3}>Rp 123.456</Text>
            </View>
            <View style={{padding:20, alignItems:'center'}}>
                <Image
                    style={styles.sub1}
                    source={require('./asset/sub1.png')}
                />
                <View style={{padding:10, alignItems:'stretch', justifyContent:'flex-start'}}>
                    <Text style={styles.text4}>- Kita Pernah Salah</Text>
                    {/* <Text style={styles.text4}>- Kita Pernah Salah</Text> */}
                </View>
            </View>
            <View style={{padding:20, alignItems:'stretch', flexDirection:'column'}}>
                <View style={{alignItems:'center'}}>
                    <Image
                        style={styles.sub1}
                        source={require('./asset/sub2.png')}
                    />
                </View>
                
                <View style={{paddingTop:20, alignItems:'center', justifyContent:'space-evenly', flexDirection:'row'}}>
                    <View style={{alignItems:'center', flex:1}}>
                        <Image
                            style={styles.logo}
                            source={require('./asset/logo-facebook.png')}
                        />
                        <Text style={styles.text4}>JanatinKhusni</Text>
                    </View>
                    <View style={{alignItems:'center', flex:1}}>
                        <Image
                            style={styles.logo}
                            source={require('./asset/logo-twitter.png')}
                        />
                        <Text style={styles.text4}>@janatinkhusni</Text>
                    </View>
                    <View style={{alignItems:'center', flex:1}}>
                        <Image
                            style={styles.logo}
                            source={require('./asset/logo-instagram.png')}
                        />
                        <Text style={styles.text4}>@janatinkhusni</Text>
                    </View>
                </View>
            </View>
            <View style={{alignItems:'stretch', flexDirection:'column', marginTop:10}}>
                <View style={{alignItems:'center', height:40}}>
                    <Image
                        style={styles.sub1}
                        source={require('./asset/sub3.png')}
                    />
                </View>
                
                <View style={{paddingTop:20, alignItems:'center', justifyContent:'space-evenly', flexDirection:'row'}}>
                    <View style={{alignItems:'center', flex:1}}>
                        <Image
                            style={styles.logo}
                            source={require('./asset/logo-bitbucket.png')}
                        />
                        <Text style={styles.text4}>janatinkhusni</Text>
                    </View>
                    <View style={{alignItems:'center', flex:1}}>
                        <Image
                            style={styles.logo}
                            source={require('./asset/logo-github.png')}
                        />
                        <Text style={styles.text4}>@janatinkhusni</Text>
                    </View>
                    <View style={{alignItems:'center', flex:1}}>
                        <Image
                            style={styles.logo}
                            source={require('./asset/logo-gitlab.png')}
                        />
                        <Text style={styles.text4}>@janatin.khusni</Text>
                    </View>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: 'flex-start',
        alignItems:'stretch',
        flexDirection:'column',
        backgroundColor:'white',
        paddingTop:30
    },
    container2:{
        alignItems:'center', 
        backgroundColor:'#E6E9F0', 
        marginHorizontal:40, 
        flexDirection:'column', 
        justifyContent:'center'
    },
    image:{
        height:100,
        width:100
    },
    text:{
        color:'#09203F',
        fontSize: 24
    },
    text2:{
        fontFamily: 'Inter_900Black',
        flex:1,
        textAlignVertical: 'center',
        color:'#09203F',
        fontSize:16,
    },
    text3:{
        fontFamily: 'Inter_900Black',
        marginLeft: 12,
        color:'#09203F',
        fontSize:16,
    },
    text4:{
        fontFamily: 'Inter_900Black',
        color:'#09203F',
        fontSize:16,
        paddingTop:4
    },
    sub1:{
        height:30,
        width:300
    },
    logo:{
        height: 52,
        width: 52,
        alignItems:'center'
    },
})